

import h2o


h2o.init()

#create test data samples
data = [(36.0 ,"Male", "Nephrology", "35.0" , "Nephrology Unit", "D1", 8.0, "1.0", "0", "LRRT", 2.0),
       (36.0, "Male", "Nephrology", "25.0", "Nephrology Unit", "AB4", "NaN", "NaN", "0", "LRRT", "NaN")]

coln = ["age","psex","department","bed_no", "unit_name","ward_name","num_adm","pre_adm", "icu","SURGERY",'charlson_index']


given_types = {'C1': 'numeric', 'C2': 'factor', 'C3': 'factor', 'C4': 'factor',
               'C5': 'factor', 'C6': 'factor', 'C7': 'numeric', 'C8': 'factor',
               'C9': 'factor', 'C10': 'factor',  'C11': 'numeric'}
test_sample = h2o.H2OFrame(data, column_types=given_types)
test_sample = test_sample.set_names(coln)
#print(test_sample)

#model load
model = h2o.load_model(path= '/Users/surabhisinghal/Documents/gitrepo/cauti_prediction_Deployment/cauti-prediction/Deployment/GBM_grid_0_AutoML_20181210_051836_model_3')

result = model.predict(test_sample)


for i in range(len(result)):
    if(result[i,'TRUE'] >= 0.0645):
        result[i,'predict'] = "TRUE"
    else:
        result[i,'predict'] = "FALSE"



print(result['TRUE'])

