install.packages("DBI")
install.packages("rJava")
install.packages("RJDBC")
install.packages('teradataR')



library(DBI)
library(rJava)
library(RJDBC)
library(RPostgreSQL)


staging_con = dbConnect(dbDriver("PostgreSQL"), dbname = "staging_store",
                        host = "10.0.10.6", port = 5432, user = "ayushig",
                        password = "ayushig#321")

postgre_con = dbConnect(dbDriver("PostgreSQL"), dbname = "aiims_hmis",
                        host = "10.0.10.6", port = 5432, user = "ayushig",
                        password = "ayushig#321")

drv <- JDBC("com.microsoft.sqlserver.jdbc.SQLServerDriver" , "sqljdbc41.jar" ,
            identifier.quote="`")
vishal_con <- dbConnect(drv, "jdbc:sqlserver://10.0.10.9:1433;databaseName=dreamproject",
                        "ayushig", "ayushig#321")



#getting a ROW COUNT OF ALL TABLES in information schema for further use in the code 

table_row <- dbGetQuery (vishal_con, "DECLARE @sql nvarchar(MAX)

SELECT
    @sql = COALESCE(@sql + ' UNION ALL ', '') +
        'SELECT
            ''' + s.name + ''' AS ''Schema'',
            ''' + t.name + ''' AS ''Table'',
            COUNT(*) AS Count
            FROM ' + QUOTENAME(s.name) + '.' + QUOTENAME(t.name)
    FROM sys.schemas s
    INNER JOIN sys.tables t ON t.schema_id = s.schema_id
    ORDER BY
        s.name,
        t.name

EXEC(@sql);")




#STEP 1: Finding VAP entries 

#getting a list of all possible column, tables and schemas for our purpose

table_aiims <- dbGetQuery(vishal_con , 'select * from information_schema.columns')
table_aiims  <- as.data.frame(table_aiims)
table_aiims <- data.table(table_aiims)

#filtering only character type cases
a <- c("char" , "varchar", "text" ,"nchar" ,"ntext")
table_aiims <- table_aiims[ DATA_TYPE %in% a ]


presence = 0
presence[1:nrow(table_aiims)] = 0
for (i in 1:nrow(table_aiims)) {
  
  
  if(table_aiims$DATA_TYPE[i]=="text" )
  {
    sql_query = "select DISTINCT CAST("
    sql_query = paste(sql_query, table_aiims$COLUMN_NAME[i], sep = "")
    sql_query = paste(sql_query, " as VARCHAR(MAX))  ", sep = "")
    sql_query = paste(sql_query, " from ", sep = "")
    sql_query = paste(sql_query, table_aiims$TABLE_SCHEMA[i], sep="")
    sql_query = paste(sql_query, table_aiims$TABLE_NAME[i], sep=".")
    try(var_present <- dbGetQuery(vishal_con,sql_query))
    if(length(var_present) > 0){
      binary = unique(grep("ventilator|mechanical ventilation|VCV|PCV|ACV
                           |CMV|SIMV|PSV|PCIRV|APRV|PRVC|PAV|PEEP|NAVA
                           |IRV|PaO2|CPAP|FiO2|VAP|
                           Ventilator Associated Pneumonia|Ventilator Associated Events|venti|VAE",var_present[,1], ignore.case = TRUE))
      if(length(binary) > 0){
        presence[i] = 1
      }
    }
  }
  else
  {
    sql_query = "select distinct "
    sql_query = paste(sql_query, table_aiims$COLUMN_NAME[i], sep = "")
    sql_query = paste(sql_query, " from ", sep = "")
    sql_query = paste(sql_query, table_aiims$TABLE_SCHEMA[i], sep="")
    sql_query = paste(sql_query, table_aiims$TABLE_NAME[i], sep=".")
    try(var_present <- dbGetQuery(vishal_con,sql_query))
    if(length(var_present) > 0){
      binary = unique(grep("ventilator|mechanical ventilation|VCV|PCV|ACV
                           |CMV|SIMV|PSV|PCIRV|APRV|PRVC|PAV|PEEP|NAVA
                           |IRV|PaO2|CPAP|FiO2|VAP|
                            Ventilator Associated Pneumonia|Ventilator Associated Events|venti|VAE",var_present[,1], ignore.case = TRUE))
      if(length(binary) > 0){
        presence[i] = 1
      }
    }
  }
}

#unique(grep("A1|A9|A6", myfile$Letter, value=TRUE, fixed=TRUE))

proc_table = cbind(table_aiims$TABLE_SCHEMA,table_aiims$TABLE_NAME,
                   table_aiims$COLUMN_NAME, presence)
proc_table <- data.frame(proc_table)


#getting columns with just string matches
proc_table_1 <- proc_table[presence==1,]
proc_table_1 <- data.table(proc_table_1)
setnames(proc_table_1 , c("V1","V2" ,"V3" ) , c("Schema","Table", "Column" ))

write.csv(proc_table_1, "~/hai_prediction/VAP/ETL_VAP_proc_with11terms.csv")

 #get row counts 
proc_table_1<- merge(proc_table_1, table_row, by=c("Schema","Table"), all.x=TRUE)
setnames(proc_table_1 , "Count" , "Table_row_count")

proc_table_1_dup <- proc_table_1
#step 1.4: null count of all columns listed in proc_table_1 to elimate further columns that are not needed 

proc_table_1_dup$specific_words <- NA
#x$null_column[1:nrow(proc_table_1)] = 0

for (i in 1:nrow(proc_table_1_dup)) {
  sql_query = "select distinct("
  sql_query = paste(sql_query, proc_table_1_dup$Column[i], sep = "")
  sql_query = paste(sql_query, ")", sep = "")
  sql_query = paste(sql_query, " from ", sep = "")
  sql_query = paste(sql_query, proc_table_1_dup$Schema[i], sep="")
  sql_query = paste(sql_query, proc_table_1_dup$Table[i], sep=".")
  sql_query = paste(sql_query, " where ", sep = "")
  sql_query = paste(sql_query, proc_table_1_dup$Column[i], sep = "")
  sql_query = paste(sql_query, " like ", sep = "")
  sql_query = paste(sql_query, " '%ventilator%' ", sep = "")
  try(var_present <- dbGetQuery(vishal_con,sql_query))
  if(length(var_present) >= 0){
    proc_table_1_dup$specific_words[i] = var_present
  }
}


proc_table_1_dup$count_unique_tcno <- NA
#x$null_column[1:nrow(proc_table_1)] = 0

for (i in 1:nrow(proc_table_1_dup)) {
  sql_query = "select count(distinct(tcno))"
  #sql_query = paste(sql_query, proc_table_1_dup$Column[i], sep = "")
  #sql_query = paste(sql_query, ")", sep = "")
  sql_query = paste(sql_query, " from ", sep = "")
  sql_query = paste(sql_query, proc_table_1_dup$Schema[i], sep="")
  sql_query = paste(sql_query, proc_table_1_dup$Table[i], sep=".")
  sql_query = paste(sql_query, " where ", sep = "")
  sql_query = paste(sql_query, proc_table_1_dup$Column[i], sep = "")
  sql_query = paste(sql_query, " like ", sep = "")
  sql_query = paste(sql_query, " '%ventilator%' ", sep = "")
  try(var_present <- dbGetQuery(vishal_con,sql_query))
  if(length(var_present) >= 0){
    proc_table_1_dup$count_unique_tcno[i] = var_present
  }
}

#removing alpha cases from unique tcno
proc_table_1_dup <- proc_table_1_dup[-grep('[[:alpha:]]', proc_table_1_dup$count_unique_tcno), ]
proc_table_1_dup$count_unique_tcno <- as.numeric(proc_table_1_dup$count_unique_tcno)
                                                  
proc_table_1_dup <- apply(proc_table_1_dup,2,as.character)
write.csv(proc_table_1_dup, "~/hai_prediction/VAP/ETL_VAP_vishal.csv")
etl_vap <- fread("~/hai_prediction/VAP/ETL_VAP_vishal.csv")

#*******************************************************************************************#
#********************************END OF ETL for VAP*****************************************#






