# VAP Prediction
To predict the patients who are highly sussceptible to VAP. 

## Project Description
- User Access and Authentication Project

| Software   |      Version  | 
|----------  |:-------------:|
| Python     |    3.6.4      |
| H2o        |   3.18.0.3 as per 10.5    |


##Deployment details
-***Model name***: StackedEnsemble_VAP
-***Python execution script***: VAP_Deployment.py
-***list of categories in input variable***: variable_category.csv



###List of input variables 

| Variable Name   |  Type       | Conditions if any |   
|-----------------|:------------|:-----------------:|
| age             | numeric     | in range 0-100    |
| psex            |  factor     | Male, Female      |
|                 |             | (no third gender) |
| department      |  factor     | no Male in Gynaecology-B, |          |                 |             | Gynaecology-C      | 
| bed_no          |   factor    |                    |
| unit_name       |   factor    |                    |
| ward_name       |   factor    |                    |
| num_adm         | numeric     |  in range 0-100    |
| pre_adm         | factor      |  1,0               |      
| icu             |   factor    |  1,0               |                 
| SURGERY         | factor      |                    |
|charlson_index   | numeric     | range(0-10)        |

###Formulas for variables 
-num_adm : this denotes previous + current admission in the hospital.
and hence FORMULA: number of previous admission+1
-pre_adm :  this denotes if a previous admission has happened or not for the same patient. FORMULA: if num_adm<2,pre_adm==0 , else pre_adm==1
-icu : this denotes if the patient is an ICU patient or not.FORMULA: if ward_name contains string 'icu',icu==1 , else 0 
-charlson index : weighted sum of comorbidies. ***pick the list of comorbidites from this formula to go in the front end***.
FORMULA:    Cancer *4 +   Cerebrovascular_Disease_or_Tran *1.5 +   Liver_Disease *2.5 +   Heart_Disease *1 +   Diabetes *1.5 +   Renal_Disease *2 +   Dementia *1 +   HIV_AIDS *6 +   Pulmonary_Disease *1 +   Connective_Tissue_Disease *1 + Hypertension *1





