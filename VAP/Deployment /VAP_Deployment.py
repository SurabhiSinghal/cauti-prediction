
# coding: utf-8

# In[19]:


import h2o
import os


h2o.init()

#create test data samples
data = [('Female',  'Medicine','1.0', 'Unit-I','C2-ICU', 'CASE OF ENCEPHALOPATHY UREMIC AND SEPTIC.TO PLAN FOR TRACHEOSTOMY IN VIEW OF PROLONGED MECHANICAL VENTILATION', 15.0 , 71.0, 'NaN',    '0.0',  'True'  ),
        ('Male', 'Gastroenterology', '8', 'Unit-I', 'D6-ICU','Right Hemicolectomy with metastatectomy', 1, 51, 4, '0', 'True')]
coln =["psex","department","bed_no", "unit_name","ward_name","SURGERY","num_adm","age",'charlson_index' , 'pre_adm', 'icu']

given_types = {'C1': 'factor', 'C2': 'factor', 'C3': 'factor', 'C4': 'factor',
               'C5': 'factor', 'C6': 'factor', 'C7': 'numeric', 'C8': 'numeric',
               'C9': 'numeric', 'C10': 'factor',  'C11': 'factor'}
test_sample = h2o.H2OFrame(data, column_types=given_types)
test_sample = test_sample.set_names(coln)
#print(test_sample)

loc = os.getcwd()
loc = loc + '/model1_SA_17dec/StackedEnsemble_AllModels_0_AutoML_20181217_144541'
#print(loc)

#model load
model = h2o.load_model(path= loc)
result = model.predict(test_sample)

'''
for i in range(len(result)):
    if(result[i,'p1'] >0.15):
        result[i,'predict'] = 1
    else:
        result[i,'predict'] = 0


'''


result['p1']

