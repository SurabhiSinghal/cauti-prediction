
import h2o
from h2o.automl import H2OAutoML
from h2o.estimators.random_forest import H2ORandomForestEstimator
from h2o.estimators.gbm import H2OGradientBoostingEstimator
from h2o.estimators.stackedensemble import H2OStackedEnsembleEstimator
from h2o.estimators.xgboost import H2OXGBoostEstimator
from h2o.grid.grid_search import H2OGridSearch
from __future__ import print_function
from sklearn.model_selection import train_test_split

from __future__ import division
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
import warnings
warnings.filterwarnings('ignore')


from sklearn.linear_model import LinearRegression, Lasso, Ridge
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
from scipy import stats
from statsmodels.stats.outliers_influence import variance_inflation_factor


hai = pd.read_csv( '/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/hai_cauti_como_3_foley.csv')

hai.head()

h2o.init()

hai = h2o.import_file(path = '/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/hai_cauti_como_2_foley.csv')


# split into train and validation sets
train2, test2 = hai.split_frame(ratios = [.8], seed = 1452)
print('train2', train2.shape)
print('test2', test2.shape)
#print('valid2', valid2.shape)
print(hai.shape)



x1=["psex","department","bed_no", "unit_name","ward_name","SURGERY","num_adm","age",'charlson_index' , 'pre_adm', 'icu']
x2 = ["psex","department","bed_no", "unit_name","ward_name","SURGERY","num_adm","age", 'pre_adm', 'icu','Cancer', 
      'Cerebrovascular_Disease_or_Tran', 'Liver_Disease', 'Heart_Disease', 'Diabetes', 'No_Comorbidity_Not_Sure', 
      'Renal_Disease','Dementia', 'HIV_AIDS', 'Pulmonary_Disease', 'Connective_Tissue_Disease']


y='dummy_growth_positive'


to_convert = ["psex","department","bed_no", "unit_name","ward_name","SURGERY", 'icu','dummy_growth_positive', 'pre_adm',
             'Cancer', 'Cerebrovascular_Disease_or_Tran', 'Liver_Disease', 'Heart_Disease', 'Diabetes', 'No_Comorbidity_Not_Sure', 
      'Renal_Disease','Dementia', 'HIV_AIDS', 'Pulmonary_Disease', 'Connective_Tissue_Disease']
to_numeric = ["num_adm","age",'charlson_index' ]
    

#def conversions(df)
for col in to_convert:
    train2[col] = train2[col].asfactor()
    test2[col] = test2[col].asfactor()
    #valid2[col] = valid2[col].asfactor() 
    
for col in to_numeric:
    train2[col]= train2[col].asnumeric()
    test2[col]= test2[col].asnumeric()
    #valid2[col] = valid2[col].asnumeric() 


aml = H2OAutoML(max_models =10, stopping_rounds=3,nfolds=10, seed=789)


aml.train(x=x1, y = y,
          training_frame = train2)


aml.leaderboard

aml.leader


perf  = aml.leader.model_performance(test2)
perf


perf.confusion_matrix(thresholds=0.0745)


print('recall' , perf.recall(thresholds=[0.0745]))
print('precision', perf.precision(thresholds=[0.0745]))
print('specificity', perf.specificity(thresholds=[0.0745]))



# Save the DRF model to disk
# the model will be saved as "./folder_for_myDRF/myDRF"
h2o.save_model(aml.leader, path = "/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Code/model1_SA_10oct") # define your path here
