
import psycopg2 as pg
import pandas.io.sql as psql
import pandas as pd
import numpy as np
import pandas_profiling

#Connecting to database
conn_hmis=pg.connect(database="aiims_hmis", user="surabhis", password="surabhis#321", host="10.0.10.6", port="5432")
conn_stag=pg.connect(database="staging_store", user="surabhis", password="surabhis#321", host="10.0.10.6", port="5432")

cur_hmis=conn_hmis.cursor()
cur_stag=conn_stag.cursor()

cur_hmis.description
cur_stag.description

#INSERTING ALL TABLES FROM STAGING STORE 

hai = psql.read_sql("select * from hai_culture_details_flat_table;",conn_stag)

#imported to get comorbidity mapping caut
los_flat = psql.read_sql("select * from los_flat_table;",conn_stag)

#getting all vishal tables as csvs because ms sql linking was an issue
#this is picking up relevant columns from the vitales table 

#select tcno, ipd_admission_id,URINE, INTAKE,FOLEY, FOLEYDATE, FOLEYDAYS , entrytime  from dbo.VITALES
#where URINE is not NULL and URINE<>'' and URINE<>'nil';

#cauti_iden = pd.read_csv('Use dummy_transfer/cauti_iden.csv')
cauti_iden = pd.read_csv('/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/cauti_iden.csv')
ot_surgryinfo = pd.read_csv("/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/ot_surg_info.csv")

#HAI unique with previous admission and number of admissions
'''
hai_id = hai[["hosp_patient_id","hosp_admission_id","admission_date","release_date"]]
hai_id.head()
hai_id =hai_id.hosp_admission_id.duplicated
hai_id = hai_id.drop_duplicates("hosp_admission_id") 
hai_id.shape

Code to calculate number of previous admissions 

def no_admission(id_patient, adm_date):
    if(adm_date is None):
        count=0
    else:
        idd= hai_id[hai_id['hosp_patient_id']==id_patient]
        dates=idd['admission_date']
        dates= dates.dropna()
        if (len(dates)>0):
            dates['prev']=dates.apply(lambda x: x<= adm_date)
            count=(dates['prev']==True).sum()
        else:
            count=0
    return count

hai_id['num_adm']=hai_id.apply(lambda x: no_admission(x['hosp_patient_id'], x['admission_date']), axis=1)

'''

#importing the num adm and pre_adm from debrat's CSV since the code took too long to run in my machine

hai_id = pd.read_csv('/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/hai_id.csv')
hai_id.head()

hai_id['pre_adm'] = np.where(hai_id['num_adm']<2,0,1)
hai_id.head()

hai = pd.merge(hai, hai_id,  how='left', left_on=["hosp_patient_id","hosp_admission_id"], right_on = ["hosp_patient_id","hosp_admission_id"])
hai.head()


#hai.dtypes
hai[["slno", "hosp_admission_id", "hosp_patient_id"]] = hai[["slno", "hosp_admission_id", "hosp_patient_id"]].fillna(value='na')

hai['x']= hai["slno"].map(str)+ hai["hosp_admission_id"] + hai["hosp_patient_id"]


#hai and urine 
hai_nd = hai.drop_duplicates("x")
hai_nd.shape
hai_nd.head()
hai_nd_urine=hai_nd[hai_nd['sample_type_desc'].str.contains("Urine")]


hai_nd_urine.head()
hai_nd_urine.shape

#los in inserted to get como
los_flat=los_flat[['los', 'hosp_patient_id', 'hosp_admission_id',
          'cancer',
       'cancer_lymph_leuk', 'cancer_metastatic_solid_tumor',
       'cerebrovascular_or_transient_ischemic_disease',
       'congestive_heart_failure', 'dementia_or_alzheimers', 'depression',
       'diabetes', 'diabetes_with_endorgandamage', 'gastric_or_peptic_ulcer',
       'hemiplegia', 'hiv_or_aids', 'hypertension', 'mild_liver_disease',
       'myocardial_infarction', 'peripheral_vascular_disease_or_bypass',
       'pulmonary_disease_asthma', 'renal_disease',
       'rheumatic_or_connective_tissue_disease', 'severe_liver_disease',
       'skin_ulcers_cellulitis',
       ]]
#cleaning cauti identification 
cauti_iden = cauti_iden.dropna(axis=0, subset=["FOLEYDATE"])
#cauti_iden.shape
cauti_iden_unq = cauti_iden.drop_duplicates("tcno") 
cauti_iden_unq.shape

cauti_iden_unq[cauti_iden_unq.tcno.isnull()]


hai_nd_urine['hosp_patient_id'] =  pd.to_numeric(hai_nd_urine['hosp_patient_id'], errors='coerce')
cauti_iden_unq['tcno']= pd.to_numeric(cauti_iden_unq['tcno'], errors='coerce')

#hai and urine merge
hai_cauti = pd.merge(hai_nd_urine, cauti_iden_unq,left_on=['hosp_patient_id'], right_on=['tcno'] )

hai_cauti['hosp_patient_id'] =  pd.to_numeric(hai_cauti['hosp_patient_id'], errors='coerce')
hai_cauti['hosp_admission_id'] =  pd.to_numeric(hai_cauti['hosp_admission_id'], errors='coerce')
los_flat['hosp_patient_id'] =  pd.to_numeric(los_flat['hosp_patient_id'], errors='coerce')
los_flat['hosp_admission_id'] =  pd.to_numeric(los_flat['hosp_admission_id'], errors='coerce')

#hai, urine, como merge
hai_cauti_como = pd.merge(hai_cauti,los_flat,  how='left', left_on=["hosp_patient_id","hosp_admission_id"], right_on = ["hosp_patient_id","hosp_admission_id"])
x = hai_cauti_como[hai_cauti_como['scient_name']!='NA'].scient_name.count()
print(x)

#subsetting the data
hai_cauti_como = hai_cauti_como[[ 'hosp_admission_id','slno','scient_name',
       'hosp_patient_id', 'age', 'psex', 'department', 'bed_no', 'unit_name',
       'ward_id', 'ward_name', 'test_name', 'sub_test_name',
       'sample_collection_datetime',
       'sample_type_desc', 
       'admission_date', 'release_date',
        'num_adm','pre_adm','entrytime','dummy_growth_positive',
       'los', 'cancer', 'cancer_lymph_leuk',
       'cancer_metastatic_solid_tumor',
       'cerebrovascular_or_transient_ischemic_disease',
       'congestive_heart_failure', 'dementia_or_alzheimers', 'depression',
       'diabetes', 'diabetes_with_endorgandamage', 'gastric_or_peptic_ulcer',
       'hemiplegia', 'hiv_or_aids', 'hypertension', 'mild_liver_disease',
       'myocardial_infarction', 'peripheral_vascular_disease_or_bypass',
       'pulmonary_disease_asthma', 'renal_disease',
       'rheumatic_or_connective_tissue_disease', 'severe_liver_disease',
       'skin_ulcers_cellulitis']]

hai_cauti_como.dtypes
hai_cauti_como['sample_collection_datetime'] = pd.to_datetime(hai_cauti_como['sample_collection_datetime'])
hai_cauti_como['admission_date'] = pd.to_datetime(hai_cauti_como['admission_date'])
hai_cauti_como['release_date'] = pd.to_datetime(hai_cauti_como['release_date'])
hai_cauti_como['entrytime'] = pd.to_datetime(hai_cauti_como['entrytime'])

'''
Function for Day sample 

def day_sample(collected, admit, release, sample_date):
   if(collected==1):
       if(pd.isnull(admit) is True):
           day=0
       else:
           day=(sample_date-admit).days
   else:
       if(admit<=release):
           day=(release-admit).days
       else:
           day=0
   return day
'''


#check for addmision date <= sample collection date <= discharge date
hai_cauti_como['check_date_samplecollection']=(((hai_cauti_como['admission_date']<= hai_cauti_como['sample_collection_datetime']) & (hai_cauti_como['sample_collection_datetime'] <= hai_cauti_como['release_date']))).apply(int)
hai_cauti_como.check_date_samplecollection.value_counts()
hai_cauti_como=hai_cauti_como[hai_cauti_como['check_date_samplecollection']==1]
hai_cauti_como.shape

#check for catheter date
hai_cauti_como['check_date_cath']=(((hai_cauti_como['admission_date']<= hai_cauti_como['entrytime']) & (hai_cauti_como['entrytime'] <= hai_cauti_como['release_date'])) | (hai_cauti_como['admission_date'].isnull()                        | hai_cauti_como['entrytime'].isnull()| hai_cauti_como['release_date'].isnull())).apply(int)
hai_cauti_como.check_date_cath.value_counts()
hai_cauti_como=hai_cauti_como[hai_cauti_como['check_date_cath']==1]
hai_cauti_como.shape

#cleaning OT surgery info

ot_surgryinfo = ot_surgryinfo[["TCNO","SCDATE","SURGERY"]]
ot_surgryinfo = ot_surgryinfo.dropna(axis=0, subset=["SCDATE"])
ot_surgryinfo["TCNO"]= pd.to_numeric(ot_surgryinfo["TCNO"], errors='coerce')
ot_surgryinfo["SCDATE"]=pd.to_datetime(ot_surgryinfo["SCDATE"])


#merge hai, urine, como, ot 
hai_cauti_como_ot = pd.merge(hai_cauti_como, ot_surgryinfo,how='left', left_on=["hosp_patient_id"], right_on = ["TCNO"])

#check for admission date <= surgery date <= discharge date
hai_cauti_como_ot['check_date_surg']=((hai_cauti_como_ot['admission_date']<= hai_cauti_como_ot['SCDATE']) & (hai_cauti_como_ot['SCDATE'] <= hai_cauti_como_ot['release_date'])).apply(int)
hai_cauti_como_ot.check_date_surg.value_counts()
hai_cauti_como_ot[hai_cauti_como_ot['check_date_surg']==0]['SURGERY']= None

#populating nulls in pre_adm& num adm
hai_cauti_como_ot['pre_adm']=hai_cauti_como_ot['pre_adm'].fillna(0)
hai_cauti_como_ot.num_adm = np.where(hai_cauti_como_ot.num_adm.isnull(),
                                         hai_cauti_como_ot.hosp_patient_id.value_counts()
                                         [hai_cauti_como_ot.hosp_patient_id], hai_cauti_como_ot.num_adm)



to_convert = ['cancer',
       'cancer_lymph_leuk', 'cancer_metastatic_solid_tumor',
       'cerebrovascular_or_transient_ischemic_disease',
       'congestive_heart_failure', 'dementia_or_alzheimers', 'depression',
       'diabetes', 'diabetes_with_endorgandamage', 'gastric_or_peptic_ulcer',
       'hemiplegia', 'hiv_or_aids', 'hypertension', 'mild_liver_disease',
       'myocardial_infarction', 'peripheral_vascular_disease_or_bypass',
       'pulmonary_disease_asthma', 'renal_disease',
       'rheumatic_or_connective_tissue_disease', 'severe_liver_disease',
       'skin_ulcers_cellulitis']


#functions to create new columns bases old columns 
def input3Calculator(cancer_lymph_leuk,cancer_metastatic_solid_tumor,cancer):
    if(cancer_lymph_leuk==1):
        return True
    if(cancer_metastatic_solid_tumor==1):
        return True
    if(cancer==1):
        return True
    if((cancer_lymph_leuk==0)and(cancer_metastatic_solid_tumor==0)and(cancer==0)):
        return False
    return None

def input2Calculator(cancer_lymph_leuk,cancer_metastatic_solid_tumor):
    if(cancer_lymph_leuk==1):
        return True
    if(cancer_metastatic_solid_tumor==1):
        return True
    if((cancer_lymph_leuk==0)and(cancer_metastatic_solid_tumor==0)):
        return False
    return None

def input1Calculator(cancer_lymph_leuk):
    if(cancer_lymph_leuk==1):
        return True
    if((cancer_lymph_leuk==0)):
        return False
    return None

hai_cauti_como_ot['Cancer']= hai_cauti_como_ot.apply(lambda x:input3Calculator(x.cancer_lymph_leuk,x.cancer_metastatic_solid_tumor, x.cancer),axis=1)
hai_cauti_como_ot['Cancer'].value_counts()

hai_cauti_como_ot['Cerebrovascular_Disease_or_Tran'] = hai_cauti_como_ot.apply(lambda x: input2Calculator(x.hemiplegia, x.cerebrovascular_or_transient_ischemic_disease), axis=1)
hai_cauti_como_ot.Cerebrovascular_Disease_or_Tran.value_counts()

hai_cauti_como_ot['Liver_Disease']= hai_cauti_como_ot.apply(lambda x: input2Calculator(x.severe_liver_disease, x.mild_liver_disease), axis=1)
hai_cauti_como_ot.Liver_Disease.value_counts()

hai_cauti_como_ot['Heart_Disease']= hai_cauti_como_ot.apply(lambda x:input3Calculator(x.congestive_heart_failure,x.myocardial_infarction, x.peripheral_vascular_disease_or_bypass),axis=1)
hai_cauti_como_ot['Heart_Disease'].value_counts()

hai_cauti_como_ot['Diabetes']= hai_cauti_como_ot.apply(lambda x: input2Calculator(x.diabetes_with_endorgandamage, x.diabetes), axis=1)
hai_cauti_como_ot.Diabetes.value_counts()

hai_cauti_como_ot['No_Comorbidity_Not_Sure']= hai_cauti_como_ot.apply(lambda x:input3Calculator(x.depression,x.gastric_or_peptic_ulcer, x.skin_ulcers_cellulitis),axis=1)
hai_cauti_como_ot['No_Comorbidity_Not_Sure'].value_counts()
  
hai_cauti_como_ot['Renal_Disease']= hai_cauti_como_ot.apply(lambda x: input1Calculator(x.renal_disease), axis=1)
hai_cauti_como_ot.Renal_Disease.value_counts()

hai_cauti_como_ot['Dementia']= hai_cauti_como_ot.apply(lambda x: input1Calculator(x.dementia_or_alzheimers), axis=1)
hai_cauti_como_ot.Dementia.value_counts()

hai_cauti_como_ot['HIV_AIDS']= hai_cauti_como_ot.apply(lambda x: input1Calculator(x.hiv_or_aids), axis=1)
hai_cauti_como_ot.HIV_AIDS.value_counts()

hai_cauti_como_ot['Pulmonary_Disease']= hai_cauti_como_ot.apply(lambda x: input1Calculator(x.pulmonary_disease_asthma), axis=1)
hai_cauti_como_ot.Pulmonary_Disease.value_counts()

hai_cauti_como_ot['Connective_Tissue_Disease']= hai_cauti_como_ot.apply(lambda x: input1Calculator(x.rheumatic_or_connective_tissue_disease), axis=1)
hai_cauti_como_ot.Connective_Tissue_Disease.value_counts()

#as given by eeshan, corrected surgery names and appending it to old data
surgery_corrected_names = pd.read_csv("/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/Corrections/eyeProcessed.csv")
surgery_corrected_names = surgery_corrected_names[['Name', 'Name2']]
surgery_corrected_names.head()
surgery_corrected_names = surgery_corrected_names.drop_duplicates()
surgery_corrected_names.shape

hai_cauti_como_ot = pd.merge(hai_cauti_como_ot, surgery_corrected_names, left_on='SURGERY', right_on='Name', how='left')
hai_cauti_como_ot = hai_cauti_como_ot.drop('Name', axis=1)
hai_cauti_como_ot['SURGERY'] = hai_cauti_como_ot['Name2']
hai_cauti_como_ot = hai_cauti_como_ot.drop('Name2', axis=1)
hai_cauti_como_ot.shape


'''
OLD CHARLSON CALCULATION -- REDUNDANT
hai_cauti_como_ot['charlson_index'] = hai_cauti_como_ot['gastric_or_peptic_ulcer']*1 + hai_cauti_como_ot['hypertension']*1 + hai_cauti_como_ot['cerebrovascular_or_transient_ischemic_disease']*1 + hai_cauti_como_ot['congestive_heart_failure']*1+\
  hai_cauti_como_ot['peripheral_vascular_disease_or_bypass']*1 + hai_cauti_como_ot['hemiplegia']*2 + hai_cauti_como_ot['myocardial_infarction']*1 + hai_cauti_como_ot['cancer']*2 + hai_cauti_como_ot['cancer_lymph_leuk']*2 +\
  hai_cauti_como_ot['cancer_metastatic_solid_tumor']*6 + hai_cauti_como_ot['dementia_or_alzheimers']*1 + hai_cauti_como_ot['skin_ulcers_cellulitis']*2 + hai_cauti_como_ot['rheumatic_or_connective_tissue_disease']*1 + hai_cauti_como_ot['depression']*1 +\
  hai_cauti_como_ot['pulmonary_disease_asthma']*1 + hai_cauti_como_ot['hiv_or_aids']*6 + hai_cauti_como_ot['severe_liver_disease']*3 + hai_cauti_como_ot['mild_liver_disease']*2 + hai_cauti_como_ot['renal_disease']*2 + hai_cauti_como_ot['diabetes']*1+\
  hai_cauti_como_ot['diabetes_with_endorgandamage']*2

'''
#CHARLSON CALUCATION 
hai_cauti_como_ot['charlson_index'] = hai_cauti_como_ot['Cancer']*4 + hai_cauti_como_ot['Cerebrovascular_Disease_or_Tran']*1.5 + hai_cauti_como_ot['Liver_Disease']*2.5 + hai_cauti_como_ot['Heart_Disease']*1 + hai_cauti_como_ot['Diabetes']*1.5 + hai_cauti_como_ot['Renal_Disease']*2 + hai_cauti_como_ot['Dementia']*1 + hai_cauti_como_ot['HIV_AIDS']*6 + hai_cauti_como_ot['Pulmonary_Disease']*1 + hai_cauti_como_ot['Connective_Tissue_Disease']*1 

#ICU
hai_cauti_como_ot['icu']=   np.where(hai_cauti_como_ot['ward_name'].str.contains("ICU"),True,False)    
#hai_cauti_como_ot[hai_cauti_como_ot['ward_name'].str.contains("icu")]

#final subsetting of data
hai_cauti_como_2 = hai_cauti_como_ot[['hosp_admission_id', 'hosp_patient_id', 'age', 'psex', 'department',
       'bed_no', 'unit_name', 'ward_id', 'ward_name', 'test_name',
       'sub_test_name', 'sample_collection_datetime', 'sample_type_desc',
       'admission_date', 'release_date', 'num_adm','pre_adm', 'entrytime',
       'dummy_growth_positive', 'los', 'Cancer', 'Cerebrovascular_Disease_or_Tran', 'Liver_Disease',
       'Heart_Disease', 'Diabetes', 'No_Comorbidity_Not_Sure', 'Renal_Disease',
       'Dementia', 'HIV_AIDS', 'Pulmonary_Disease',
       'Connective_Tissue_Disease', 'charlson_index', 'icu', 'SURGERY']]


hai_cauti_como_2.to_csv('/Users/surabhisinghal/Documents/gitrepo/hai/CAUTI/Data/dummy_transfer/hai_cauti_como_2_foley.csv')
